# Note about docker in docker (dind)

Important note about docker in docker (dind) with the gitlab runner. 
You need to define dind service inside the ci pipeline and run the runner container in privileged mode. 
```
2019-08-02T06:45:42.567628475Z failed to load listeners: listen tcp: lookup docker on 169.254.169.254:53: no such host
```
Don’t forget --docker-privileged and keep docker and docker-dind versions coherent:

```
job:login:
  image: docker:latest
  stage: login
  tags:
    - docker-latest-dind
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script: ....
```

Working with the – docker-privileged of same version latest
```
gitlab-runner register -n \
  --non-interactive \
  --url https://YOUR_gitlab.com/ \
  --registration-token YOUR_REGISTRATION_TOKEN \
  --name YOUR_RUNNER_NAME
  --tag-list "docker-latest-dind" \
  --executor docker \
  --docker-image "docker:latest" \
  --docker-privileged
```

======== you can precise the version it is working with 19.03.12 or 18.09.7 ====
```
job:login19:
  image: docker:19.03.12
  stage: login
  tags:
    - docker-19.03.12-dind
  services:
    - docker:19.03.12-dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script: ....
```

Working with the – docker-privileged of same version ex: 19.03.12
```
gitlab-runner register -n \
  --non-interactive \
  --url https://YOUR_gitlab.com/ \
  --registration-token YOUR_REGISTRATION_TOKEN \
  --name YOUR_RUNNER_NAME
  --tag-list "docker-19.03.12-dind" \
  --executor docker \
  --docker-image "docker:19.03.12" \
  --docker-privileged
```

###  OC taken from 

https://mirror.openshift.com/pub/openshift-v3/clients/3.11.248/linux/



