FROM gitlab/gitlab-runner
MAINTAINER admin@dataandstoragesolutions.com

ENV USER='gitlab-runner' TOKEN='' OSHIFT_URL='' OSHIFT_ID='' OSHIFT_PW='' 
#OSHIFT_PW could be password or token.

RUN wget https://mirror.openshift.com/pub/openshift-v3/clients/3.11.248/linux/oc.tar.gz && \
 tar -xvf ./oc.tar.gz && cp ./oc /usr/bin/oc && rm -f ./oc.tar.gz && chmod +x /usr/bin/oc

RUN mkdir -p /home/gitlab-runner/.gitlab-runner && touch /home/gitlab-runner/.gitlab-runner/config.toml && \
 chown 999:999 -R /home/gitlab-runner/.gitlab-runner/

USER gitlab-runner

VOLUME ["/home/gitlab-runner/.gitlab-runner"]
